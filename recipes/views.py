from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from recipes.models import Recipe, Ingredient
from recipes.forms import RecipeForm, IngredientForm

# Create your views here.
def show_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request,"recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request,"recipes/list.html", context)

@login_required(login_url='login')
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request,"recipes/list.html", context)

@login_required(login_url='login')
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
    context = {
        "form": form,
    }
    return render(request, "recipes/create.html", context)

@login_required(login_url='login')
def edit_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    # ingredient = Ingredient.objects.get(id=id)

    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            # form2.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
        # form2 = IngredientForm(instance=ingredient)

    context = {
        "recipe_object": recipe,
        "recipe_form": form,
        # "ingredient-object": ingredient,
        # "ingredient_form": form,
    }
    return render(request, "recipes/edit.html", context)

# def edit_ingredients(request, id):
#     ingredient = Ingredient.objects.get(id=id)
#     # recipes = Recipe.objects.filter(-something here -)
#     if request.method == "POST":
#         form = IngredientForm(request.POST, instance=ingredient)
#         if form.is_valid():
#             form.save()
#             return redirect("show_recipe", id=id)
#     else:
#         form = IngredientForm(instance=ingredient)
#     context = {
#         "ingredient_object": ingredient,
#         "ingredient_form": form,
#     }
#     return render(request, "recipes/edit.html", context)
